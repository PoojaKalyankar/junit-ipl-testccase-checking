package org.example;
import java.util.*;
import junit.framework.TestCase;

public class MainTest extends TestCase {
    Main obj=new Main();
    List<String[]>matches=obj.readCsv("/home/pooja/IdeaProjects/iplProjectJUnit/src/matches.csv");
    List<String[]>delivaries=obj.readCsv("/home/pooja/IdeaProjects/iplProjectJUnit/src/deliveries.csv");
    public void testMatchesPlayerPerYear() {
        HashMap<String,Integer> hashMap=new HashMap<>();
        hashMap.put("2008",58);
        hashMap.put("2009",57);
        hashMap.put("2010",60);
        hashMap.put("2011",73);
        hashMap.put("2012",74);
        hashMap.put("2013",76);
        hashMap.put("2014",60);
        hashMap.put("2015",59);
        hashMap.put("2016",60);
        hashMap.put("2017",59);
        assertNotNull(obj.matchesPlayerPerYear(matches));
        assertTrue(obj.matchesPlayerPerYear(matches).size()==10);
        assertEquals(hashMap,obj.matchesPlayerPerYear(matches));
    }
    public void testMatchesWonAllTeams() {
        HashMap<String,Integer> hashMap=new HashMap<>();
        hashMap.put("",3);
        hashMap.put("Mumbai Indians",92);
        hashMap.put("Sunrisers Hyderabad",42);
        hashMap.put("Pune Warriors",12);
        hashMap.put("Rajasthan Royals",63);
        hashMap.put("Kolkata Knight Riders",77);
        hashMap.put("Royal Challengers Bangalore",73);
        hashMap.put("Gujarat Lions",13);
        hashMap.put("Rising Pune Supergiant",10);
        hashMap.put("Kochi Tuskers Kerala",6);
        hashMap.put("Kings XI Punjab",70);
        hashMap.put("Deccan Chargers",29);
        hashMap.put("Delhi Daredevils",62);
        hashMap.put("Rising Pune Supergiants",5);
        hashMap.put("Chennai Super Kings",79);
        assertNotNull(obj.matchesWonAllTeams(matches));
        assertFalse(obj.matchesWonAllTeams(matches).size()<=14);
        assertEquals(hashMap,obj.matchesWonAllTeams(matches));
    }

    public void testExtraRuns2016PerTeam() {
        HashMap<String,Integer>hashMap=new HashMap<>();
        hashMap.put("Gujarat Lions",98);
        hashMap.put("Mumbai Indians",102);
        hashMap.put("Sunrisers Hyderabad",107);
        hashMap.put("Kings XI Punjab",100);
        hashMap.put("Delhi Daredevils",106);
        hashMap.put("Rising Pune Supergiants",108);
        hashMap.put("Kolkata Knight Riders",122);
        hashMap.put("Royal Challengers Bangalore",156);
        assertNotNull(obj.extraRuns2016PerTeam(matches,delivaries));
        assertFalse(obj.extraRuns2016PerTeam(matches,delivaries).size()>14);
        assertEquals(hashMap,obj.extraRuns2016PerTeam(matches,delivaries));
    }

    public void testTop10EconomicalBowler2015() {
        HashMap<String,Double>hmap=new HashMap<>();
        hmap.put("RN ten Doeschate",3.4285714285714284);
        hmap.put("J Yadav",4.142857142857143);
        hmap.put("V Kohli",5.454545454545455);
        hmap.put("R Ashwin",5.725);
        hmap.put("S Nadeem",5.863636363636364);
        hmap.put("Z Khan",6.15483870967742);
        hmap.put("Parvez Rasool",6.2);
        hmap.put("MC Henriques",6.2675159235668785);
        hmap.put("MA Starc",6.75);
        hmap.put("M de Lange",6.923076923076923);
        assertNotNull(obj.top10EconomicalBowler2015(matches,delivaries));
        assertEquals(hmap,obj.top10EconomicalBowler2015(matches,delivaries));
        assertTrue(obj.top10EconomicalBowler2015(matches,delivaries).size()==10);
    }

    public void testTeamWonBothTossMatchForEveryYear() {
      HashMap<String,Integer>expected=new HashMap<>();
      expected.put("Mumbai Indians",48);
      expected.put("Sunrisers Hyderabad",17);
      expected.put("Pune Warriors",3);
      expected.put("Rajasthan Royals",34);
      expected.put("Kolkata Knight Riders",44);
      expected.put("Royal Challengers Bangalore",35);
      expected.put("Gujarat Lions",10);
      expected.put("Rising Pune Supergiant",5);
      expected.put("Kochi Tuskers Kerala",4);
      expected.put("Kings XI Punjab",28);
      expected.put("Deccan Chargers",19);
      expected.put("Delhi Daredevils",33);
      expected.put("Rising Pune Supergiants",3);
      expected.put("Chennai Super Kings",42);
      assertNull(obj.teamWonBothTossMatchForEveryYear(matches));
      assertSame(expected,obj.teamWonBothTossMatchForEveryYear(matches));
      assertTrue(obj.teamWonBothTossMatchForEveryYear(matches).size()==14);
    }
}